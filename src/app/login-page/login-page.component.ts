import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(public snackBar: MatSnackBar) { }
  openSnackBar() {
    this.snackBar.open("hello", "hi", {
      duration: 2000,
    });
  }
  ngOnInit() {
  }

}
